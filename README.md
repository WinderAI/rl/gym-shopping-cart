# gym-display-advertising

An OpenAI Gym for Shopping Cart Reinforcement Learning.

This is a project by [Winder Research](https://WinderResearch.com), a Cloud-Native Data Science consultancy.

## Installation

`pip install gym-shopping-cart`

## Usage

This example will use the small example data included in the repo.

```python
import gym
import gym_shopping_cart

env = gym.make("ShoppingCart-v0")
episode_over = False
rewards = 0
while not episode_over:
    state, reward, episode_over, _ = env.step(env.action_space.sample())
    print(state, reward)
    rewards += reward
print("Total reward: {}".format(rewards))
```

## Real Shopping Cart Data

This environment uses real shopping cart information from the [Instacart dataset](https://tech.instacart.com/3-million-instacart-orders-open-sourced-d40d29ead6f2).

To help read this data the library also comes with a data parser. This loads the raw data and cleans the data to be in a format expected by the environment.

### Update On the Data

It looks like instacart have removed the data: https://www.instacart.com/datasets/grocery-shopping-2017 no longer exists and the s3 bucket is returning a 403: https://s3.amazonaws.com/instacart-datasets/instacart_online_grocery_shopping_2017_05_01.tar.gz

So thank goodness that I had a local copy of the data on my laptop! Phew. I've uploaded that to a new location. The whole data is about 200MB zipped up. There's more information about this data on kaggle: https://www.kaggle.com/c/instacart-market-basket-analysis/ but you can download it from here:

```sh
!wget -N https://s3.eu-west-2.amazonaws.com/assets.winderresearch.com/data/instacart_online_grocery_shopping_2017_05_01.tar.gz
```

## Credits

Gitlab icon made by [Freepik](https://www.flaticon.com/authors/freepik) from [www.flaticon.com](https://www.flaticon.com/).
